﻿using System;

namespace shipwreck
{
    public static class Extensions
    {
        /// <summary>
        /// Extensionmethode zum Inkrementieren einer Enumeration.
        /// </summary>
        /// <typeparam name="T">Typ muss eine Enumeration sein.</typeparam>
        /// <param name="source">Der Parameter muss von Typ einer Enumeration sein.</param>
        /// <returns></returns>
        public static T Next<T>(this T source) where T : struct
        {
            if (typeof(T).IsEnum)
            {
                T[] TArray = (T[])Enum.GetValues(source.GetType());
                int i = Array.IndexOf(TArray, source) + 1;
                if (i == TArray.Length)
                {
                    return TArray[0];
                }
                else
                {
                    return TArray[i];
                }
            }
            else
            {
                throw new ArgumentException(String.Format("{0} is not an Enum", typeof(T).FullName));
            }
        }

        /// <summary>
        /// Extensionmethode zum Rückgabe eines Zufälligen Wertes einer Enumeration.
        /// </summary>
        /// <typeparam name="T">Typ muss eine Enumeration sein.</typeparam>
        /// <param name="source">Der Parameter muss von Typ einer Enumeration sein.</param>
        /// <returns></returns>
        public static T Random<T>(this T source) where T : struct
        {
            Random rnd = new Random();

            T[] TArray = (T[])Enum.GetValues(source.GetType());

            return TArray[rnd.Next(0, TArray.Length)];
        }
    }
}