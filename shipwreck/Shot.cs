﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace shipwreck
{
    internal class Shot
    {
        private Canvas canvas;
        private Image explosion;
        private int explosionProgress = 1;
        private DispatcherTimer explosionTimer;
        private int groundSize;
        private bool hit = false;
        private Image hitMarker;
        private int positionX;
        private int positionY;
        private Image waterMarker;

        /// <summary>
        /// Initialisiert eine neue Instanz der Shot-Klasse.
        /// </summary>
        /// <param name="x">X-Koordinate des Schusses.</param>
        /// <param name="y">X-Koordinate des Schusses.</param>
        /// <param name="groundSize">Größe des Spielfeldes.</param>
        /// <param name="target">Ship-Liste, mit der eine Kollision festgestellt werden soll.</param>
        /// <param name="canvas">Canvas-Objekt auf dem sich die Schiffe und damit auch der Schuss befinden soll.</param>
        public Shot(int x, int y, int groundSize, List<Ship> target, Canvas canvas)
        {
            positionX = x;
            positionY = y;
            this.groundSize = groundSize;
            this.canvas = canvas;

            explosionTimer = new DispatcherTimer()
            {
                Interval = new TimeSpan(0, 0, 0, 0, 85)
            };
            explosionTimer.Tick += Explosion;

            foreach (Ship ship in target)
            {
                if (ship.CollisionHit(x, y))
                {
                    ship.Hit();
                    hit = true;
                    break;
                }
            }

            explosionTimer.Start();
        }

        public int GetX()
        {
            return positionX;
        }

        public int GetY()
        {
            return positionY;
        }

        public bool IsHit()
        {
            return hit;
        }

        // Erzeugt eine Explosions-Animation falls eine Kollision mit einem Schiff festgestellt wurde.
        // Falls keine Kollision mit einem Schiff festgestellt wurde, verweist die Methode sobald der
        // Timer 5 mal durchgelaufen ist auf die SetHitMarker()-Methode.
        private void Explosion(object sender, EventArgs e)
        {
            if (explosion != null)
            {
                canvas.Children.Remove(explosion);
            }

            if (hit && explosionProgress <= 5)
            {
                explosion = new Image()
                {
                    Height = (700 / groundSize),
                    Width = (700 / groundSize),
                    Source = (ImageSource)System.Windows.Application.Current.Resources["explosion" + explosionProgress.ToString()],
                    Margin = new System.Windows.Thickness(positionX * (700 / groundSize), positionY * (700 / groundSize), 0, 0),
                    Opacity = 0.9
                };

                Panel.SetZIndex(explosion, 7);
                canvas.Children.Add(explosion);
            }

            if (explosionProgress == 5)
            {
                explosionTimer.Stop();
                canvas.Children.Remove(explosion);
                SetHitMarker();
            }
            else
            {
                explosionProgress++;
            }
        }

        // Setzt einen Marker für einen Treffer ins Wasser oder ein Schiffstreffer fest.
        private void SetHitMarker()
        {
            if (hit)
            {
                hitMarker = new Image()
                {
                    Height = (700 / groundSize),
                    Width = (700 / groundSize),
                    Source = (ImageSource)System.Windows.Application.Current.Resources["shipMarker"],
                    Margin = new System.Windows.Thickness(positionX * (700 / groundSize), positionY * (700 / groundSize), 0, 0),
                    Opacity = 0.6
                };
                Panel.SetZIndex(hitMarker, 7);
                canvas.Children.Add(hitMarker);
            }
            else
            {
                waterMarker = new Image()
                {
                    Height = (700 / groundSize),
                    Width = (700 / groundSize),
                    Source = (ImageSource)System.Windows.Application.Current.Resources["waterMarker"],
                    Margin = new System.Windows.Thickness(positionX * (700 / groundSize), positionY * (700 / groundSize), 0, 0)
                };
                Panel.SetZIndex(waterMarker, 7);
                canvas.Children.Add(waterMarker);
            }
        }
    }
}