﻿using System.Windows;
using System.Windows.Input;

namespace shipwreck
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        private Menu menu;

        public MainWindow()
        {
            InitializeComponent();

            var version =   System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." +
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor + "." +
                            System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build;

            LabelVersion.Content = "Version " + version.ToString() + " - Ansuz";

            Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["standard"]).Cursor;
            menu = new Menu(MainGrid);
        }

        private void Btn_quit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }

        private void Btn_single_Click(object sender, RoutedEventArgs e)
        {
            menu.Singleplayer();
        }
    }
}