﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace shipwreck
{
    internal class GhostShip
    {
        private Rectangle ghostShip;
        private Canvas canvas;
        private int shipSize;
        private int groundSize;

        private int[] coords;

        /// <summary>
        /// Initialisiert eine neue Instanz der GhostShip-Klasse.
        /// Ghostships werden verwendet um eine Vorschau zu erzeugen, die mit der Maus wandert.
        /// </summary>
        /// <param name="shipSize">Größe des zu erzeugenden Ghostships.</param>
        /// <param name="groundSize">Größe des Spielfeldes.</param>
        /// <param name="direction">Ausrichtung des Schiffs.</param>
        /// <param name="canvas">Canvas-Objekt auf dem das Schiff abgebildet werden soll.</param>
        public GhostShip(int shipSize, int groundSize, Ship.Direction direction, Canvas canvas)
        {
            this.shipSize = shipSize;
            this.groundSize = groundSize;
            this.canvas = canvas;

            coords = new int[2];

            ghostShip = new Rectangle()
            {
                Width = 700 / groundSize,
                Height = (700 / groundSize) * shipSize,
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Visibility = Visibility.Hidden
            };

            switch (shipSize)
            {
                case 2:
                    ghostShip.Fill = new ImageBrush((ImageSource)Application.Current.Resources["ship_small_1a"]);
                    break;

                case 3:
                    ghostShip.Fill = new ImageBrush((ImageSource)Application.Current.Resources["ship_middle_1a"]);
                    break;

                case 4:
                    ghostShip.Fill = new ImageBrush((ImageSource)Application.Current.Resources["ship_large_1a"]);
                    break;
            }

            Rotate(direction);

            Panel.SetZIndex(ghostShip, 20);
            canvas.Children.Add(ghostShip);
        }

        public void ToggleVisibility()
        {
            if (ghostShip.IsVisible)
            {
                ghostShip.Visibility = Visibility.Hidden;
            }
            else
            {
                ghostShip.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Aktuallisiert die Position des GhostShip-Objektes.
        /// </summary>
        /// <param name="x">X-Koordinate der neuen Position.</param>
        /// <param name="y">Y-Koordinate der neuen Position.</param>
        public void Move(int x, int y)
        {
            switch (shipSize)
            {
                case 2:
                    ghostShip.Fill = new ImageBrush((ImageSource)Application.Current.Resources["ship_small_1a"]);
                    break;

                case 3:
                    ghostShip.Fill = new ImageBrush((ImageSource)Application.Current.Resources["ship_middle_1a"]);
                    break;

                case 4:
                    ghostShip.Fill = new ImageBrush((ImageSource)Application.Current.Resources["ship_large_1a"]);
                    break;
            }
            ghostShip.Margin = new Thickness(x, y, 0, 0);
        }

        public void Rotate(Ship.Direction direction)
        {
            double angle = 0.0;
            switch (direction)
            {
                case Ship.Direction.Bottom:
                    angle = 0.0;
                    break;

                case Ship.Direction.Left:
                    angle = 90.0;
                    break;

                case Ship.Direction.Top:
                    angle = 180.0;
                    break;

                case Ship.Direction.Right:
                    angle = 270.0;
                    break;
            }
            RotateTransform rotateTransform = new RotateTransform(angle, ((700 / groundSize) / 2), ((700 / groundSize) / 2));
            ghostShip.RenderTransform = rotateTransform;
        }

        public void Remove()
        {
            canvas.Children.Remove(ghostShip);
            ghostShip = null;
        }
    }
}