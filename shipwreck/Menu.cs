﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace shipwreck
{
    internal class Menu
    {
        private byte groundsize = 6;
        private byte shipcount_small = 1;
        private byte shipcount_middle = 1;
        private byte shipcount_large = 1;
        private byte CheckCountDivider = 4;

        private bool tutorial = false;

        private TextBlock Tb_GroundSize;
        private TextBlock Tb_ShipSmall_Count;
        private TextBlock Tb_ShipMiddle_Count;
        private TextBlock Tb_ShipLarge_Count;

        private Grid settinggrid = new Grid()
        {
            Height = 1080,
            Width = 1920
        };

        private Image scroll = new Image()
        {
            Source = (ImageSource)Application.Current.Resources["bg_scroll"],
            Width = 1920,
            Height = 1080
        };

        private Button Btn_close = new Button()
        {
            Width = 80,
            Height = 80,
            HorizontalAlignment = HorizontalAlignment.Right,
            VerticalAlignment = VerticalAlignment.Top,
            Margin = new Thickness(0, 170, 430, 0),
            BorderBrush = null,
            Style = (Style)Application.Current.Resources["Btn_X_Style"]
        };

        private TextBlock Tb_Headline = new TextBlock()
        {
            Background = Brushes.Transparent,
            Width = 1000,
            Height = 150,
            Text = "Fehler",
            FontSize = 90,
            HorizontalAlignment = HorizontalAlignment.Left,
            VerticalAlignment = VerticalAlignment.Top,
            Margin = new Thickness(540, 150, 0, 0),
            Style = (Style)Application.Current.Resources["Setting_Headline"]
        };

        public Menu(Grid grid)
        {
            Btn_close.Click += Btn_close_Click;
            Panel.SetZIndex(settinggrid, 3);
            Panel.SetZIndex(scroll, 1);
            Panel.SetZIndex(Btn_close, 5);
            Panel.SetZIndex(Tb_Headline, 2);
            grid.Children.Add(settinggrid);
        }

        private void Standard()
        {
            settinggrid.Children.Add(scroll);
            settinggrid.Children.Add(Btn_close);
            settinggrid.Children.Add(Tb_Headline);
        }

        public void Singleplayer()
        {
            if (!(settinggrid.Children.Count > 0))
            {
                Tb_Headline.Text = "Singleplayer";
                Standard();

                Button Btn_NewGameSingle = new Button()
                {
                    Background = new ImageBrush((ImageSource)Application.Current.Resources["btn_plank"]),
                    Width = 500,
                    Height = 100,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Margin = new Thickness(700, 850, 0, 0),
                    BorderBrush = null,
                    Style = (Style)Application.Current.Resources["Btn_Plank_Style"],
                    Content = "Starte Spiel"
                };
                TextBlock Tb_Headline_Field = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 800,
                    Height = 100,
                    Margin = new Thickness(675, 270, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 60,
                    Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(("#FF080707"))),
                    Text = "Schlachtfeld:"
                };
                TextBlock Tb_Headline_Ship = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 800,
                    Height = 100,
                    Margin = new Thickness(650, 460, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 60,
                    Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(("#FF080707"))),
                    Text = "Schiffsanzahl:"
                };

                TextBlock Tb_GroundSize_Tag = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 220,
                    Height = 100,
                    Margin = new Thickness(515, 380, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = "Größe:"
                };
                Tb_GroundSize = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 100,
                    Height = 100,
                    Margin = new Thickness(745, 380, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = groundsize.ToString()
                };
                Button Btn_GroundSizeIncreas = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "+",
                    Margin = new Thickness(815, 370, 0, 0)
                };
                Button Btn_GroundSizeDecrease = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "-",
                    Margin = new Thickness(815, 410, 0, 0)
                };

                TextBlock Tb_ShipSmall_Count_Tag = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 200,
                    Height = 100,
                    Margin = new Thickness(740, 770, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = "Klein:"
                };
                Tb_ShipSmall_Count = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 100,
                    Height = 100,
                    Margin = new Thickness(955, 770, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = shipcount_middle.ToString()
                };
                Button Btn_ShipSmall_Count_Increas = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "+",
                    Margin = new Thickness(1060, 755, 0, 0)
                };
                Button Btn_ShipSmall_Count_Decrease = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "-",
                    Margin = new Thickness(1060, 795, 0, 0)
                };

                TextBlock Tb_ShipMiddle_Count_Tag = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 220,
                    Height = 100,
                    Margin = new Thickness(690, 670, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = "Mittel:"
                };
                Tb_ShipMiddle_Count = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 100,
                    Height = 100,
                    Margin = new Thickness(960, 670, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = shipcount_large.ToString()
                };
                Button Btn_ShipMiddle_Count_Increas = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "+",
                    Margin = new Thickness(1060, 660, 0, 0)
                };
                Button Btn_ShipMiddle_Count_Decrease = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "-",
                    Margin = new Thickness(1060, 700, 0, 0)
                };

                TextBlock Tb_ShipLarge_Count_Tag = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 220,
                    Height = 100,
                    Margin = new Thickness(735, 570, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = "Groß:"
                };
                Tb_ShipLarge_Count = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 100,
                    Height = 100,
                    Margin = new Thickness(965, 570, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = shipcount_large.ToString()
                };
                Button Btn_ShipLarge_Count_Increas = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "+",
                    Margin = new Thickness(1060, 565, 0, 0)
                };
                Button Btn_ShipLarge_Count_Decrease = new Button()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 60,
                    Height = 40,
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 25,
                    Content = "-",
                    Margin = new Thickness(1060, 605, 0, 0)
                };

                CheckBox CbTutorial = new CheckBox()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 50,
                    Height = 50,
                    Margin = new Thickness(1315, 390, 0, 0),
                    Style = (Style)Application.Current.Resources["CircleCheckbox"]
                };
                TextBlock TbTutoral = new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = 300,
                    Height = 100,
                    Margin = new Thickness(1015, 380, 0, 0),
                    FontFamily = (FontFamily)Application.Current.Resources["wreck"],
                    FontSize = 45,
                    Text = "Tutorial:"
                };

                Panel.SetZIndex(Btn_NewGameSingle, 2);
                Panel.SetZIndex(Tb_Headline_Field, 2);
                Panel.SetZIndex(Tb_Headline_Ship, 2);
                Panel.SetZIndex(Tb_GroundSize_Tag, 2);
                Panel.SetZIndex(Tb_GroundSize, 2);
                Panel.SetZIndex(Btn_GroundSizeIncreas, 2);
                Panel.SetZIndex(Btn_GroundSizeDecrease, 2);
                Panel.SetZIndex(Tb_ShipSmall_Count_Tag, 2);
                Panel.SetZIndex(Tb_ShipSmall_Count, 2);
                Panel.SetZIndex(Btn_ShipSmall_Count_Increas, 2);
                Panel.SetZIndex(Btn_ShipSmall_Count_Decrease, 2);
                Panel.SetZIndex(Tb_ShipMiddle_Count_Tag, 2);
                Panel.SetZIndex(Tb_ShipMiddle_Count, 2);
                Panel.SetZIndex(Btn_ShipMiddle_Count_Increas, 2);
                Panel.SetZIndex(Btn_ShipMiddle_Count_Decrease, 2);
                Panel.SetZIndex(Tb_ShipLarge_Count_Tag, 2);
                Panel.SetZIndex(Tb_ShipLarge_Count, 2);
                Panel.SetZIndex(Btn_ShipLarge_Count_Increas, 2);
                Panel.SetZIndex(Btn_ShipLarge_Count_Decrease, 2);
                Panel.SetZIndex(CbTutorial, 2);
                Panel.SetZIndex(TbTutoral, 2);

                Btn_NewGameSingle.Click += Btn_NewGameSingle_CLick;
                Btn_GroundSizeIncreas.Click += Btn_GroundSizeIncreas_CLick;
                Btn_GroundSizeDecrease.Click += Btn_GroundSizeDecrease_CLick;
                Btn_ShipSmall_Count_Increas.Click += Btn_ShipSmall_Count_Increas_Click;
                Btn_ShipSmall_Count_Decrease.Click += Btn_ShipSmall_Count_Decrease_Click;
                Btn_ShipMiddle_Count_Increas.Click += Btn_ShipMiddle_Count_Increas_Click;
                Btn_ShipMiddle_Count_Decrease.Click += Btn_ShipMiddle_Count_Decrease_Click;
                Btn_ShipLarge_Count_Increas.Click += Btn_ShipLarge_Count_Increas_Click;
                Btn_ShipLarge_Count_Decrease.Click += Btn_ShipLarge_Count_Decrease_Click;

                CbTutorial.Checked += CbTutorialCheckChanged;
                CbTutorial.Unchecked += CbTutorialCheckChanged;

                settinggrid.Children.Add(Btn_NewGameSingle);
                settinggrid.Children.Add(Tb_Headline_Field);
                settinggrid.Children.Add(Tb_Headline_Ship);
                settinggrid.Children.Add(Tb_GroundSize_Tag);
                settinggrid.Children.Add(Tb_GroundSize);
                settinggrid.Children.Add(Btn_GroundSizeIncreas);
                settinggrid.Children.Add(Btn_GroundSizeDecrease);
                settinggrid.Children.Add(Tb_ShipSmall_Count_Tag);
                settinggrid.Children.Add(Tb_ShipSmall_Count);
                settinggrid.Children.Add(Btn_ShipSmall_Count_Increas);
                settinggrid.Children.Add(Btn_ShipSmall_Count_Decrease);
                settinggrid.Children.Add(Tb_ShipMiddle_Count_Tag);
                settinggrid.Children.Add(Tb_ShipMiddle_Count);
                settinggrid.Children.Add(Btn_ShipMiddle_Count_Increas);
                settinggrid.Children.Add(Btn_ShipMiddle_Count_Decrease);
                settinggrid.Children.Add(Tb_ShipLarge_Count_Tag);
                settinggrid.Children.Add(Tb_ShipLarge_Count);
                settinggrid.Children.Add(Btn_ShipLarge_Count_Increas);
                settinggrid.Children.Add(Btn_ShipLarge_Count_Decrease);
                settinggrid.Children.Add(CbTutorial);
                settinggrid.Children.Add(TbTutoral);
            }
        }

        /// <summary>
        /// Methode zur Überprüfung der Schiffsanzahl in Abhängigkeit von der Spielfeldgröße.
        /// Als Parameter wird ein Wert für das Objekt übergeben, bei dem sich die Anzahl ändert.
        /// </summary>
        /// <param name="value">Auswahl: 1 - Kleines Schiff, 2 - mittelgroßes Schiff, 3 - großes Schiff, 4 - Feld</param>
        private void CheckCount(int value)
        {
            while (((groundsize * groundsize) / CheckCountDivider) < shipcount_small * 2 + shipcount_middle * 3 + shipcount_large * 4)
            {
                switch (value)
                {
                    case 1:
                        if (shipcount_large >= shipcount_middle)
                        {
                            if (((groundsize * groundsize) / CheckCountDivider) < (shipcount_small * 2 + shipcount_middle * 3 + shipcount_large * 4))
                            {
                                if (shipcount_large > 0)
                                {
                                    shipcount_large--;
                                }
                            }
                        }
                        if (shipcount_middle > shipcount_large)
                        {
                            if (((groundsize * groundsize) / CheckCountDivider) < (shipcount_small * 2 + shipcount_middle * 3 + shipcount_large * 4))
                            {
                                if (shipcount_middle > 0)
                                {
                                    shipcount_middle--;
                                }
                            }
                        }
                        if (shipcount_middle == 0 && shipcount_large == 0 && shipcount_small > 1)
                        {
                            shipcount_small--;
                        }
                        break;

                    case 2:
                        if (shipcount_large >= shipcount_small)
                        {
                            if (((groundsize * groundsize) / CheckCountDivider) < (shipcount_small * 2 + shipcount_middle * 3 + shipcount_large * 4))
                            {
                                if (shipcount_large > 0)
                                {
                                    shipcount_large--;
                                }
                            }
                        }
                        if (shipcount_small > shipcount_large)
                        {
                            if (((groundsize * groundsize) / CheckCountDivider) < (shipcount_small * 2 + shipcount_middle * 3 + shipcount_large * 4))
                            {
                                if (shipcount_small > 0)
                                {
                                    shipcount_small--;
                                }
                            }
                        }
                        if (shipcount_small == 0 && shipcount_large == 0 && shipcount_middle > 1)
                        {
                            shipcount_middle--;
                        }
                        break;

                    case 3:
                        if (shipcount_small >= shipcount_middle)
                        {
                            if (((groundsize * groundsize) / CheckCountDivider) < (shipcount_small * 2 + shipcount_middle * 3 + shipcount_large * 4))
                            {
                                if (shipcount_small > 0)
                                {
                                    shipcount_small--;
                                }
                            }
                        }
                        if (shipcount_middle > shipcount_small)
                        {
                            if (((groundsize * groundsize) / CheckCountDivider) < (shipcount_small * 2 + shipcount_middle * 3 + shipcount_large * 4))
                            {
                                if (shipcount_middle > 0)
                                {
                                    shipcount_middle--;
                                }
                            }
                        }
                        if (shipcount_middle == 0 && shipcount_small == 0 && shipcount_large > 1)
                        {
                            shipcount_large--;
                        }
                        break;

                    case 4:
                        if (shipcount_large >= shipcount_middle && shipcount_large >= shipcount_small)
                        {
                            if (shipcount_large > 0)
                            {
                                shipcount_large--;
                            }
                        }
                        else if (shipcount_middle > shipcount_large && shipcount_middle > shipcount_small)
                        {
                            if (shipcount_middle > 0)
                            {
                                shipcount_middle--;
                            }
                        }
                        else
                        {
                            if (shipcount_small > 0)
                            {
                                shipcount_small--;
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private void Btn_NewGameSingle_CLick(object sender, RoutedEventArgs e)
        {
            Battlefield battlefield = new Battlefield(groundsize, shipcount_small, shipcount_middle, shipcount_large, tutorial);
            battlefield.Show();
        }

        private void Btn_GroundSizeIncreas_CLick(object sender, RoutedEventArgs e)
        {
            if (groundsize < 16)
                groundsize += 2;
            CheckCount(4);
            TextBoxRefresh();
        }

        private void Btn_GroundSizeDecrease_CLick(object sender, RoutedEventArgs e)
        {
            if (groundsize > 6)
                groundsize -= 2;
            CheckCount(4);
            TextBoxRefresh();
        }

        private void Btn_ShipSmall_Count_Increas_Click(object sender, RoutedEventArgs e)
        {
            if (shipcount_small < 10)
                shipcount_small++;
            CheckCount(1);
            TextBoxRefresh();
        }

        private void Btn_ShipSmall_Count_Decrease_Click(object sender, RoutedEventArgs e)
        {
            if (shipcount_small > 0)
                shipcount_small--;
            CheckCount(1);
            TextBoxRefresh();
        }

        private void Btn_ShipMiddle_Count_Increas_Click(object sender, RoutedEventArgs e)
        {
            if (shipcount_middle < 10)
                shipcount_middle++;
            CheckCount(2);
            TextBoxRefresh();
        }

        private void Btn_ShipMiddle_Count_Decrease_Click(object sender, RoutedEventArgs e)
        {
            if (shipcount_middle > 0)
                shipcount_middle--;
            CheckCount(2);
            TextBoxRefresh();
        }

        private void Btn_ShipLarge_Count_Increas_Click(object sender, RoutedEventArgs e)
        {
            if (shipcount_large < 10)
                shipcount_large++;
            CheckCount(3);
            TextBoxRefresh();
        }

        private void Btn_ShipLarge_Count_Decrease_Click(object sender, RoutedEventArgs e)
        {
            if (shipcount_large > 0)
                shipcount_large--;
            CheckCount(3);
            TextBoxRefresh();
        }

        private void CbTutorialCheckChanged(object sender, RoutedEventArgs e)
        {
            tutorial = !tutorial;
        }

        private void Btn_close_Click(object sender, RoutedEventArgs e)
        {
            settinggrid.Children.Clear();
        }

        private void TextBoxRefresh()
        {
            Tb_GroundSize.Text = groundsize.ToString();
            Tb_ShipSmall_Count.Text = shipcount_small.ToString();
            Tb_ShipMiddle_Count.Text = shipcount_middle.ToString();
            Tb_ShipLarge_Count.Text = shipcount_large.ToString();
        }
    }
}