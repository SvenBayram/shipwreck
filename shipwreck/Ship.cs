﻿//#define RotationOld
#define RotationNew

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace shipwreck
{
    internal class Ship
    {
        private int groundSize;
        private int shipSize;
        private Canvas canvas;
        private bool isDead = false;
        private int[,] coords;
        private int[,] range;
        private int lifePoints;

        private Image ship;

        public bool IsDead { get => isDead; set => isDead = value; }

        public enum Direction
        {
            Bottom,
            Left,
            Top,
            Right
        }

        public Ship(int groundSize, int shipSize, Canvas canvas, int x, int y, Direction direction)
        {
            this.groundSize = groundSize;
            this.shipSize = shipSize;
            this.canvas = canvas;
            lifePoints = shipSize;

            coords = new int[shipSize, 2];
            range = new int[(shipSize * 2) + 2, 2];

            //Initialisierung des Int-Arrays mit einem Wert der keine Collision verursacht, wenn diese Stelle nicht belegt werden würde.
            //Standard-Initialisierung ist 0. 0 ist aber eine Koordinate auf dem Spielfeld. Also einfach den Wert auf -1 gesetzt, da
            //die Koordinate -1 keine Relevanz für das Spiel hat.
            for (int i = 0; i < range.Length / 2; i++)
            {
                range[i, 0] = -1;
                range[i, 1] = -1;
            }
            switch (direction)
            {
                case Direction.Bottom:
                    if (y > 0)
                    {
                        range[0, 0] = x;
                        range[0, 1] = y - 1;
                    }
                    else
                    {
                        range[0, 0] = x;
                        range[0, 1] = y;
                    }
                    if (y < groundSize - shipSize)
                    {
                        range[1, 0] = x;
                        range[1, 1] = y + shipSize;
                    }
                    else
                    {
                        range[1, 0] = x;
                        range[1, 1] = y;
                    }
                    for (int i = 0; i < shipSize; i++)
                    {
                        coords[i, 0] = x;
                        coords[i, 1] = y + i;

                        if (x + 1 < groundSize)
                        {
                            range[i + 2, 0] = x + 1;
                            range[i + 2, 1] = y + i;
                        }
                        if (x - 1 >= 0)
                        {
                            range[i + 2 + shipSize, 0] = x - 1;
                            range[i + 2 + shipSize, 1] = y + i;
                        }
                    }
                    break;

                case Direction.Left:
                    if (x > shipSize - 1)
                    {
                        range[0, 0] = x - shipSize;
                        range[0, 1] = y;
                    }
                    else
                    {
                        range[0, 0] = x;
                        range[0, 1] = y;
                    }
                    if (x < groundSize - 1)
                    {
                        range[1, 0] = x + 1;
                        range[1, 1] = y;
                    }
                    else
                    {
                        range[1, 0] = x;
                        range[1, 1] = y;
                    }
                    for (int i = 0; i < shipSize; i++)
                    {
                        coords[i, 0] = x - i;
                        coords[i, 1] = y;

                        if (y + 1 < groundSize)
                        {
                            range[i + 2, 0] = x - i;
                            range[i + 2, 1] = y + 1;
                        }
                        if (y - 1 >= 0)
                        {
                            range[i + 2 + shipSize, 0] = x - i;
                            range[i + 2 + shipSize, 1] = y - 1;
                        }
                    }
                    break;

                case Direction.Top:
                    if (y > shipSize - 1)
                    {
                        range[0, 0] = x;
                        range[0, 1] = y - shipSize;
                    }
                    else
                    {
                        range[0, 0] = x;
                        range[0, 1] = y;
                    }
                    if (y < groundSize - 1)
                    {
                        range[1, 0] = x;
                        range[1, 1] = y + 1;
                    }
                    else
                    {
                        range[1, 0] = x;
                        range[1, 1] = y;
                    }
                    for (int i = 0; i < shipSize; i++)
                    {
                        coords[i, 0] = x;
                        coords[i, 1] = y - i;

                        if (x + 1 < groundSize)
                        {
                            range[i + 2, 0] = x + 1;
                            range[i + 2, 1] = y - i;
                        }
                        if (x - 1 >= 0)
                        {
                            range[i + 2 + shipSize, 0] = x - 1;
                            range[i + 2 + shipSize, 1] = y - i;
                        }
                    }
                    break;

                case Direction.Right:
                    if (x > 0)
                    {
                        range[0, 0] = x - 1;
                        range[0, 1] = y;
                    }
                    else
                    {
                        range[0, 0] = x;
                        range[0, 1] = y;
                    }
                    if (x + shipSize < groundSize)
                    {
                        range[1, 0] = x + shipSize;
                        range[1, 1] = y;
                    }
                    else
                    {
                        range[1, 0] = x;
                        range[1, 1] = y;
                    }
                    for (int i = 0; i < shipSize; i++)
                    {
                        coords[i, 0] = x + i;
                        coords[i, 1] = y;

                        if (y + 1 < groundSize)
                        {
                            range[i + 2, 0] = x + i;
                            range[i + 2, 1] = y + 1;
                        }
                        if (y - 1 >= 0)
                        {
                            range[i + 2 + shipSize, 0] = x + i;
                            range[i + 2 + shipSize, 1] = y - 1;
                        }
                    }
                    break;
            }
            ship = new Image()
            {
                Width = 700 / groundSize,
                Height = (700 / groundSize) * shipSize,
                Margin = new Thickness(GetXCoords(0), GetYCoords(0), 0, 0)
            };
            switch (shipSize)
            {
                case 2:
                    ship.Source = (ImageSource)Application.Current.Resources["ship_small_1a"];
                    break;

                case 3:
                    ship.Source = (ImageSource)Application.Current.Resources["ship_middle_1a"];
                    break;

                case 4:
                    ship.Source = (ImageSource)Application.Current.Resources["ship_large_1a"];
                    break;
            }

            Rotate(direction);

            Panel.SetZIndex(ship, 7);
            this.canvas.Children.Add(ship);
        }

#if DEBUG

        // Debug: Erzeugen von Markern auf allen vom Schiff und der umgebenden Fläche
        // belegten Platz, der bei der Kollisionsberechnung beachtet werden soll.
        // Die Kollision hatte sich Anfans ungewöhnlich verhalten. Daher eine Kontrollausgabe
        // welche Koordinaten ein Schiff belegen sollte.
        public void Debug()
        {
            for (int i = 0; i < coords.Length / 2; i++)
            {
                DebugDot(coords[i, 0], coords[i, 1]);
            }
            for (int i = 0; i < range.Length / 2; i++)
            {
                DebugDot(range[i, 0], range[i, 1]);
            }
        }

        private void DebugDot(int x, int y)
        {
            System.Windows.Shapes.Ellipse ellipse = new System.Windows.Shapes.Ellipse
            {
                Width = (700 / groundSize) / 5,
                Height = (700 / groundSize) / 5,
                Margin = new Thickness(((700 / groundSize) * (x + 0.5)) - (700 / groundSize) / 10, ((700 / groundSize) * (y + 0.5)) - (700 / groundSize) / 10, 0, 0),
                Fill = Brushes.Red
            };
            Panel.SetZIndex(ellipse, 5);
            canvas.Children.Add(ellipse);
        }

#endif

        public void ToggleVisibility()
        {
            if (ship.IsVisible)
            {
                ship.Visibility = Visibility.Hidden;
            }
            else
            {
                ship.Visibility = Visibility.Visible;
            }
        }

        public void Rotate(Direction direction)
        {
            double angle = 0.0;
            switch (direction)
            {
                case Direction.Bottom:
                    angle = 0.0;
                    break;

                case Direction.Left:
                    angle = 90.0;
                    break;

                case Direction.Top:
                    angle = 180.0;
                    break;

                case Direction.Right:
                    angle = 270.0;
                    break;
            }
            RotateTransform rotateTransform = new RotateTransform(angle, ((700 / groundSize) / 2), ((700 / groundSize) / 2));
            ship.RenderTransform = rotateTransform;
        }

        /// <summary>
        /// Abfrage ob sich dieses Schiff, inklusive der nächsten umliegenden Felder (nicht über Eck), an den gegebenen Parametern befindet.
        /// </summary>
        /// <param name="x">Zu prüfende X-Koordinate.</param>
        /// <param name="y">Zu prüfende Y-Koordinate.</param>
        /// <returns>True, wenn die Koordinate belegt ist.</returns>
        public bool Collision(int x, int y)
        {
            for (int i = 0; i < shipSize; i++)
            {
                if (x == coords[i, 0] && y == coords[i, 1])
                {
                    return true;
                }
            }
            for (int i = 0; i < (range.Length / 2); i++)
            {
                if (x == range[i, 0] && y == range[i, 1])
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Abfrage ob sich dieses Schiff, exklusive der nächsten umliegenden Felder (nicht über Eck), an den gegebenen Parametern befindet.
        /// </summary>
        /// <param name="x">Zu prüfende X-Koordinate.</param>
        /// <param name="y">Zu prüfende Y-Koordinate.</param>
        /// <returns>True, wenn die Koordinate belegt ist.</returns>
        public bool CollisionHit(int x, int y)
        {
            for (int i = 0; i < shipSize; i++)
            {
                if (x == coords[i, 0] && y == coords[i, 1])
                {
                    return true;
                }
            }
            return false;
        }

        private int GetXCoords(int slot)
        {
            int coordX = (700 / groundSize) * coords[slot, 0];

            return coordX;
        }

        private int GetYCoords(int slot)
        {
            int coordY = (700 / groundSize) * coords[slot, 1];

            return coordY;
        }

        public void Hit()
        {
            lifePoints--;
            if (lifePoints == 0)
            {
                isDead = true;

                if (!ship.IsVisible)
                {
                    ToggleVisibility();
                }
            }
        }
    }
}