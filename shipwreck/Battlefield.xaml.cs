﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace shipwreck
{
    /// <summary>
    /// Interaktionslogik für Battlefield.xaml.
    /// </summary>
    public partial class Battlefield : Window
    {
        private List<Ship> shipsPlayer = new List<Ship>();
        private List<Ship> shipsEnemy = new List<Ship>();

        private List<Shot> shotsPlayer = new List<Shot>();
        private List<Shot> shotsEnemy = new List<Shot>();

        private Point mouseOnPlayerField;
        private Point mouseOnEnemyField;
        private Point mouseOnBattleGrid;

        private Random rnd;

        private Rectangle compass;
        private Rectangle roulette;

        private GhostShip ghostShip;

        private DispatcherTimer gameTimer;
        private DispatcherTimer tutorialTimer;

        private System.Timers.Timer rotationTimer;
        private System.Timers.Timer winTimer;

        private RotateTransform rotateTransform;

        private enum GameLogicState
        {
            EnemyPlacing,
            PlayerShot,
            EnemyShot,
            Stop,
            PlayerStart,
            EnemyStart
        }

        private enum TutorialState
        {
            Hello,
            PlaceShipOne,
            PlaceShipTwo,
            Roulette,
            FirstTurnPlayer,
            FirstTurnEnemy,
            Stop
        }

        private GameLogicState gameLogicState = GameLogicState.Stop;
        private TutorialState tutorialState = TutorialState.Hello;
        private Ship.Direction direction = Ship.Direction.Bottom;

        private bool tutorial;

        private bool isSelected = false;
        private bool shooting = false;
        private bool shootAgain = false;

        private int selectionSize = 0;

        private bool rouletteStart = true;
        private bool rouletteActive = false;
        private double rouletteSpeed = 0.1;
        private double rouletteAngle;

        private int shipStartCountPlayerSmall;
        private int shipStartCountPlayerMiddle;
        private int shipStartCountPlayerLarge;

        private int shipStartCountEnemySmall;
        private int shipStartCountEnemyMiddle;
        private int shipStartCountEnemyLarge;

        private bool playerShoots = false;

        private int groundSize;

        /// <summary>
        /// Erstellt ein neues Spielfeld mit der Interaktionslogik.
        /// </summary>
        /// <param name="groundSize">Größe des Spielfeldes.</param>
        /// <param name="small">Anzahl der kleinen Schiffe.</param>
        /// <param name="middle">Anzahl der mittelgroßen Schiffe.</param>
        /// <param name="large">Anzahl der großen Schiffe.</param>
        /// <param name="tutorial">True, wenn man ein Tutorial zusätzlich starten möchte.</param>
        public Battlefield(int groundSize, int small, int middle, int large, bool tutorial)
        {
            InitializeComponent();
            rnd = new Random(Guid.NewGuid().GetHashCode());
            this.groundSize = groundSize;
            this.tutorial = tutorial;

            shipStartCountPlayerSmall = small;
            shipStartCountPlayerMiddle = middle;
            shipStartCountPlayerLarge = large;

            shipStartCountEnemySmall = small;
            shipStartCountEnemyMiddle = middle;
            shipStartCountEnemyLarge = large;

            TbSmallCount.Text = shipStartCountPlayerSmall.ToString();
            TbMiddleCount.Text = shipStartCountPlayerMiddle.ToString();
            TbLargeCount.Text = shipStartCountPlayerLarge.ToString();

            for (int i = 1; i < groundSize; i++)
            {
                Line lineVertical = new Line()
                {
                    X1 = (700 / groundSize) * i,
                    Y1 = 0,
                    X2 = (700 / groundSize) * i,
                    Y2 = 700,
                    Stroke = new SolidColorBrush(Color.FromRgb(8, 7, 7)),
                    StrokeThickness = 3,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                };
                Line lineHorizontal = new Line()
                {
                    X1 = 0,
                    Y1 = (700 / groundSize) * i,
                    X2 = 700,
                    Y2 = (700 / groundSize) * i,
                    Stroke = new SolidColorBrush(Color.FromRgb(8, 7, 7)),
                    StrokeThickness = 3,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                };
                Panel.SetZIndex(lineVertical, 4);
                Panel.SetZIndex(lineHorizontal, 4);
                CanvasPlayer.Children.Add(lineVertical);
                CanvasPlayer.Children.Add(lineHorizontal);
            }
            for (int i = 1; i < groundSize; i++)
            {
                Line lineVertical = new Line()
                {
                    X1 = (700 / groundSize) * i,
                    Y1 = 0,
                    X2 = (700 / groundSize) * i,
                    Y2 = 700,
                    Stroke = new SolidColorBrush(Color.FromRgb(8, 7, 7)),
                    StrokeThickness = 3,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                };

                Line lineHorizontal = new Line()
                {
                    X1 = 0,
                    Y1 = (700 / groundSize) * i,
                    X2 = 700,
                    Y2 = (700 / groundSize) * i,
                    Stroke = new SolidColorBrush(Color.FromRgb(8, 7, 7)),
                    StrokeThickness = 3,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                };

                Panel.SetZIndex(lineVertical, 4);
                Panel.SetZIndex(lineHorizontal, 4);
                CanvasEnemy.Children.Add(lineVertical);
                CanvasEnemy.Children.Add(lineHorizontal);
            }

            gameTimer = new DispatcherTimer();
            gameTimer.Interval = new TimeSpan(0, 0, 0, 0, 750);
            gameTimer.Tick += GameTimerTick;

            tutorialTimer = new DispatcherTimer();
            tutorialTimer.Interval = new TimeSpan(0, 0, 0, 1, 500);
            tutorialTimer.Tick += TutorialTimerTick;
            if (tutorial)
            {
                tutorialState = TutorialState.Hello;
                BubbleText.Text = "ArRrr\r\nWillkommen in der Schwarzwasser-\r\nbucht!";
                SpeechBubble.Visibility = Visibility.Visible;
                BubbleText.Visibility = Visibility.Visible;
                Pirate.Visibility = Visibility.Visible;
                tutorialTimer.Start();
            }
        }

        public int GetXCoords(int column)
        {
            int coords = (700 / groundSize) * column;

            return coords;
        }

        public int GetYCoords(int row)
        {
            int coords = (700 / groundSize) * row;

            return coords;
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            // Erfassen der Mausposition im Bezug zu den einzelnen Canvas-Flächen
            mouseOnPlayerField = e.GetPosition(CanvasPlayer);
            mouseOnEnemyField = e.GetPosition(CanvasEnemy);
            mouseOnBattleGrid = e.GetPosition(CanvasBattlefield);

            // Wenn ein Schiff zum Plazieren Ausgewählt wurde, entsteht ein Schiff das am Maus-Cursor hängt und
            // mit der Maus bewegt bis es plaziert wurde.
            if (isSelected)
            {
                ghostShip.Move((int)mouseOnBattleGrid.X - ((700 / groundSize) / 2), (int)mouseOnBattleGrid.Y - ((700 / groundSize) / 2));
            }
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Falls beim linken Mausklick ein Schiff selektiert ist, dann wird hier die Koordinaten der Maus erfasst und ein neues Schiffsobjekt
            // in der passenden Schiffsliste erstellt
            if (isSelected)
            {
                for (int i = 0; i < groundSize; i++)
                {
                    for (int j = 0; j < groundSize; j++)
                    {
                        if (mouseOnPlayerField.X >= GetXCoords(j) && mouseOnPlayerField.X < (GetXCoords(j) + (700 / groundSize)) && mouseOnPlayerField.Y >= GetYCoords(i) && mouseOnPlayerField.Y < (GetYCoords(i) + (700 / groundSize)) && Collision(j, i, selectionSize, direction, shipsPlayer))
                        {
                            shipsPlayer.Add(new Ship(groundSize, selectionSize, CanvasPlayer, j, i, direction));
#if DEBUG
                            shipsPlayer[shipsPlayer.Count - 1].Debug();
#endif
                            isSelected = false;

                            Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["standard"]).Cursor;

                            CheckShipPlacing();
                            ghostShip.Remove();
                            ghostShip = null;
                        }
                    }
                }
            }
            if (playerShoots)
            {
                PlayerShot();
            }
        }

        private void Window_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            // Falls ein Schiff ausgewählt ist, wird es hier durch einen Rechtsklick gedreht.
            // Desweiteren ändert sich der Cursor zum passend gredrehtem Cursor
            if (isSelected)
            {
                direction = direction.Next();
                ghostShip.Rotate(direction);
                switch (direction)
                {
                    case Ship.Direction.Bottom:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeBottom"]).Cursor;
                        break;

                    case Ship.Direction.Left:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeLeft"]).Cursor;
                        break;

                    case Ship.Direction.Top:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeTop"]).Cursor;
                        break;

                    case Ship.Direction.Right:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeRight"]).Cursor;
                        break;
                }
            }
        }

        /// <summary>
        /// Methode zur Kollisionsberechnung von Ship-Objekten.
        /// </summary>
        /// <param name="x">Die zu überprüfende X-Koordinate.</param>
        /// <param name="y">Die zu überprüfende Y-Koordinate.</param>
        /// <param name="shipsize">Größe des zu überprüfenden Schiffs.</param>
        /// <param name="direction">Ausrichtung des Schiffs.</param>
        /// <param name="ships">Liste der Schiffe mit der die Kollision überprüft werden soll.</param>
        /// <returns>Gibt True zurücck wenn keine Kollision aufgetreten ist.</returns>
        private bool Collision(int x, int y, int shipsize, Ship.Direction direction, List<Ship> ships)
        {
            switch (direction)
            {
                case Ship.Direction.Bottom:
                    for (int s = 1; s < 4; s++)
                    {
                        if (shipsize == 1 + s && y >= groundSize - s)
                        {
                            return false;
                        }
                    }
                    foreach (Ship ship in ships)
                    {
                        for (int s = 0; s < shipsize; s++)
                        {
                            if (ship.Collision(x, y + s))
                            {
                                return false;
                            }
                        }
                    }
                    break;

                case Ship.Direction.Left:
                    for (int s = 1; s < 4; s++)
                    {
                        if (shipsize == 1 + s && x <= s - 1)
                        {
                            return false;
                        }
                    }
                    foreach (Ship ship in ships)
                    {
                        for (int s = 0; s < shipsize; s++)
                        {
                            if (ship.Collision(x - s, y))
                            {
                                return false;
                            }
                        }
                    }
                    break;

                case Ship.Direction.Top:
                    for (int s = 1; s < 4; s++)
                    {
                        if (shipsize == 1 + s && y <= s - 1)
                        {
                            return false;
                        }
                    }
                    foreach (Ship ship in ships)
                    {
                        for (int s = 0; s < shipsize; s++)
                        {
                            if (ship.Collision(x, y - s))
                            {
                                return false;
                            }
                        }
                    }
                    break;

                case Ship.Direction.Right:
                    for (int s = 1; s < 4; s++)
                    {
                        if (shipsize == 1 + s && x >= groundSize - s)
                        {
                            return false;
                        }
                    }
                    foreach (Ship ship in ships)
                    {
                        for (int s = 0; s < shipsize; s++)
                        {
                            if (ship.Collision(x + s, y))
                            {
                                return false;
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
            return true;
        }

        private void TutorialTimerTick(object sender, EventArgs e)
        {
            switch (tutorialState)
            {
                case TutorialState.Hello:
                    tutorialState = TutorialState.PlaceShipOne;
                    break;

                case TutorialState.PlaceShipOne:
                    BubbleText.Text = "Klicke nun auf die Schiffe um sie auf dem linken Feld zu plazieren";
                    break;

                case TutorialState.PlaceShipTwo:
                    BubbleText.Text = "Mit dem Rechtsklick kannst du die Schiffe drehen.";
                    break;

                case TutorialState.Roulette:
                    Panel.SetZIndex(Pirate, 7);
                    Panel.SetZIndex(BubbleText, 7);
                    Panel.SetZIndex(SpeechBubble, 7);

                    Canvas.SetTop(Pirate, 330);
                    Canvas.SetLeft(Pirate, 100);
                    Canvas.SetTop(BubbleText, 80);
                    Canvas.SetLeft(BubbleText, 420);
                    Canvas.SetTop(SpeechBubble, 20);
                    Canvas.SetLeft(SpeechBubble, 300);

                    ScaleTransform scale = new ScaleTransform();
                    scale.ScaleX = 1;
                    scale.ScaleY = 1;
                    SpeechBubble.RenderTransform = scale;
                    TutorialToggleVisibility();
                    BubbleText.Text = "Wer darf anfangen? Drücke Stop um es auszulosen!";
                    tutorialTimer.Stop();
                    break;

                case TutorialState.FirstTurnPlayer:
                    BubbleText.Text = "Drücke auf Schießen und wähle einen Bereich auf dem linken Spielfeld";
                    TutorialToggleVisibility();
                    tutorialTimer.Stop();
                    break;

                case TutorialState.FirstTurnEnemy:
                    tutorialTimer.Stop();
                    break;

                default:
                    break;
            }
        }

        private void Btn_Small_Click(object sender, RoutedEventArgs e)
        {
            if (!isSelected && shipStartCountPlayerSmall != 0)
            {
                CursorRotation(shipStartCountPlayerSmall, 2);
                shipStartCountPlayerSmall--;

                ghostShip = new GhostShip(2, groundSize, direction, CanvasBattlefield);
                ghostShip.Move((int)mouseOnBattleGrid.X - ((700 / groundSize) / 2), (int)mouseOnBattleGrid.Y - ((700 / groundSize) / 2));
                ghostShip.ToggleVisibility();

                if (tutorialState == TutorialState.PlaceShipOne)
                {
                    tutorialState = TutorialState.PlaceShipTwo;
                }
            }
        }

        private void Btn_Middle_Click(object sender, RoutedEventArgs e)
        {
            if (!isSelected && shipStartCountPlayerMiddle != 0)
            {
                CursorRotation(shipStartCountPlayerMiddle, 3);
                shipStartCountPlayerMiddle--;

                ghostShip = new GhostShip(3, groundSize, direction, CanvasBattlefield);
                ghostShip.Move((int)mouseOnBattleGrid.X - ((700 / groundSize) / 2), (int)mouseOnBattleGrid.Y - ((700 / groundSize) / 2));
                ghostShip.ToggleVisibility();

                if (tutorialState == TutorialState.PlaceShipOne)
                {
                    tutorialState = TutorialState.PlaceShipTwo;
                }
            }
        }

        private void Btn_Large_Click(object sender, RoutedEventArgs e)
        {
            if (!isSelected && shipStartCountPlayerLarge != 0)
            {
                CursorRotation(shipStartCountPlayerLarge, 4);
                shipStartCountPlayerLarge--;

                ghostShip = new GhostShip(4, groundSize, direction, CanvasBattlefield);
                ghostShip.Move((int)mouseOnBattleGrid.X - ((700 / groundSize) / 2), (int)mouseOnBattleGrid.Y - ((700 / groundSize) / 2));
                ghostShip.ToggleVisibility();

                if (tutorialState == TutorialState.PlaceShipOne)
                {
                    tutorialState = TutorialState.PlaceShipTwo;
                }
            }
        }

        private void CursorRotation(int shipToPlace, int size)
        {
            if (!isSelected && shipToPlace != 0)
            {
                switch (direction)
                {
                    case Ship.Direction.Bottom:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeBottom"]).Cursor;
                        break;

                    case Ship.Direction.Left:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeLeft"]).Cursor;
                        break;

                    case Ship.Direction.Top:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeTop"]).Cursor;
                        break;

                    case Ship.Direction.Right:
                        Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["placeRight"]).Cursor;
                        break;
                }

                isSelected = true;
                selectionSize = size;
            }
        }

        /// <summary>
        /// Überprüft ob noch Schiffe zum Plazieren vorhanden sind. Falls nicht wird der gameTimer gestartet.
        /// </summary>
        private void CheckShipPlacing()
        {
            TbSmallCount.Text = shipStartCountPlayerSmall.ToString();
            TbMiddleCount.Text = shipStartCountPlayerMiddle.ToString();
            TbLargeCount.Text = shipStartCountPlayerLarge.ToString();

            if (shipStartCountPlayerSmall == 0 && shipStartCountPlayerMiddle == 0 && shipStartCountPlayerLarge == 0)
            {
                TbSmallCount.Visibility = Visibility.Hidden;
                TbMiddleCount.Visibility = Visibility.Hidden;
                TbLargeCount.Visibility = Visibility.Hidden;

                Tbx1.Visibility = Visibility.Hidden;
                Tbx2.Visibility = Visibility.Hidden;
                Tbx3.Visibility = Visibility.Hidden;

                BtnSmall.Visibility = Visibility.Hidden;
                BtnMiddle.Visibility = Visibility.Hidden;
                BtnLarge.Visibility = Visibility.Hidden;

                SpeechBubble.Visibility = Visibility.Hidden;
                BubbleText.Visibility = Visibility.Hidden;
                Pirate.Visibility = Visibility.Hidden;

                HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Der Gegner plaziert seine Schiffe!!"; }));
                gameLogicState = GameLogicState.EnemyPlacing;
                gameTimer.Start();
            }
        }

        private void GameTimerTick(object sender, EventArgs e)
        {
            switch (gameLogicState)
            {
                case GameLogicState.PlayerStart:
                    BtnShoot.Dispatcher.BeginInvoke(new Action(() => { BtnShoot.Visibility = Visibility.Visible; }));
                    gameTimer.Stop();
                    break;

                case GameLogicState.EnemyStart:
                    EnemyShot();
                    BubbleText.Dispatcher.BeginInvoke(new Action(() => { BubbleText.Text = "Drücke auf Schießen und wähle einen Bereich auf dem rechten Spielfeld"; }));
                    break;

                case GameLogicState.EnemyPlacing:
                    EnemyPlacing();
                    break;

                case GameLogicState.EnemyShot:
                    if (!shootAgain)
                    {
                        HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Der Gegner ist dran!!"; }));
                    }
                    shootAgain = false;
                    EnemyShot();
                    break;

                case GameLogicState.PlayerShot:
                    if (!shootAgain)
                    {
                        HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Du bist dran!!"; }));
                    }
                    shootAgain = false;
                    BtnShoot.Dispatcher.BeginInvoke(new Action(() => { BtnShoot.Visibility = Visibility.Visible; }));
                    gameTimer.Stop();
                    break;
            }
        }

        // Plazierung der gegnerischen Schiffe auf dem gegnerischen Feld.
        private void EnemyPlacing()
        {
            gameTimer.Stop();
            int x;
            int y;
            Ship.Direction d = Ship.Direction.Bottom;
            for (int i = 0; i < shipStartCountEnemyLarge; i++)
            {
                x = rnd.Next(1000) % groundSize;
                y = rnd.Next(1000) % groundSize;
                d = d.Random();

                if (Collision(x, y, 4, d, shipsEnemy))
                {
                    shipsEnemy.Add(new Ship(groundSize, 4, CanvasEnemy, x, y, d));
                    shipsEnemy[shipsEnemy.Count - 1].ToggleVisibility();
                }
                else
                {
                    i--;
                }
            }
            Thread.Sleep(500);
            for (int i = 0; i < shipStartCountEnemyMiddle; i++)
            {
                x = rnd.Next(1000) % groundSize;
                y = rnd.Next(1000) % groundSize;
                d = d.Random();

                if (Collision(x, y, 3, d, shipsEnemy))
                {
                    shipsEnemy.Add(new Ship(groundSize, 3, CanvasEnemy, x, y, d));
                    shipsEnemy[shipsEnemy.Count - 1].ToggleVisibility();
                }
                else
                {
                    i--;
                }
            }
            Thread.Sleep(500);
            for (int i = 0; i < shipStartCountEnemySmall; i++)
            {
                x = rnd.Next(1000) % groundSize;
                y = rnd.Next(1000) % groundSize;
                d = d.Random();

                if (Collision(x, y, 2, d, shipsEnemy))
                {
                    shipsEnemy.Add(new Ship(groundSize, 2, CanvasEnemy, x, y, d));
                    shipsEnemy[shipsEnemy.Count - 1].ToggleVisibility();
                }
                else
                {
                    i--;
                }
            }

            FadeRectangle.Visibility = Visibility.Visible;

            StartGame();
        }

        private void BtnShoot_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["crosshair"]).Cursor;

            playerShoots = true;
        }

        // Start der Interaktionslogik. Zunächst Aufruf des Roulettes zur Entscheidung wer mit dem Spiel beginnt.
        private void StartGame()
        {
            rotationTimer = new System.Timers.Timer()
            {
                Interval = 15
            };
            rotationTimer.Elapsed += Roulette;

            winTimer = new System.Timers.Timer()
            {
                Interval = 1000
            };
            winTimer.Elapsed += CheckWin;
            winTimer.Start();

            BtnStop.Visibility = Visibility.Visible;

            HeadLine.Text = "Wer fängt an?";
            compass = new Rectangle()
            {
                Width = 300,
                Height = 300,
                Margin = new Thickness(810, 390, 0, 0),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Fill = new ImageBrush((ImageSource)Application.Current.Resources["compass"])
            };
            roulette = new Rectangle()
            {
                Width = 50,
                Height = 280,
                Margin = new Thickness(935, 400, 0, 0),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Fill = new ImageBrush((ImageSource)Application.Current.Resources["needle"])
            };
            rotateTransform = new RotateTransform(0, 25, 140);

            rotationTimer.Start();

            if (tutorial)
            {
                tutorialState = TutorialState.Roulette;
            }

            Panel.SetZIndex(compass, 7);
            Panel.SetZIndex(roulette, 8);
            CanvasBattlefield.Children.Add(compass);
            CanvasBattlefield.Children.Add(roulette);
            Dimmer.Visibility = Visibility.Visible;
        }

        // Roulette zur Entscheidung, wer mit dem Spiel beginnt.
        private void Roulette(object sender, ElapsedEventArgs e)
        {
            if (rouletteStart)
            {
                roulette.Dispatcher.BeginInvoke(new Action(() => { roulette.RenderTransform = rotateTransform; }));
                rouletteAngle += rouletteSpeed;
                rotateTransform.Dispatcher.BeginInvoke(new Action(() => { rotateTransform.Angle = rouletteAngle; }));
                rouletteSpeed += 0.1;
            }
            if (rouletteActive)
            {
                roulette.Dispatcher.BeginInvoke(new Action(() => { roulette.RenderTransform = rotateTransform; }));
                rouletteAngle += rouletteSpeed;
                rotateTransform.Dispatcher.BeginInvoke(new Action(() => { rotateTransform.Angle = rouletteAngle; }));
            }
            if (!rouletteActive && !rouletteStart)
            {
                roulette.Dispatcher.BeginInvoke(new Action(() => { roulette.RenderTransform = rotateTransform; }));
                rouletteAngle += rouletteSpeed;
                rotateTransform.Dispatcher.BeginInvoke(new Action(() => { rotateTransform.Angle = rouletteAngle; }));
                rouletteSpeed -= 0.1;
            }
            if (rouletteSpeed <= 0.0)
            {
                rotationTimer.Stop();
                BtnStop.Dispatcher.BeginInvoke(new Action(() => { BtnStop.Visibility = Visibility.Hidden; }));
                rotateTransform.Dispatcher.BeginInvoke(new Action(() => { rouletteAngle = rotateTransform.Angle; }));

                if (rouletteAngle % 360 <= 180)
                {
                    BubbleText.Dispatcher.BeginInvoke(new Action(() => { BubbleText.Text = "\r\nDein Gegner fängt an!!!   ArrrRr"; }));
                    Thread.Sleep(1500);
                    RouletteToggleVisibility();
                    gameLogicState = GameLogicState.EnemyStart;
                    if (tutorial)
                    {
                        tutorialState = TutorialState.FirstTurnEnemy;
                        TutorialToggleVisibility();
                        BubbleText.Dispatcher.BeginInvoke(new Action(() => { BubbleText.Text = "Drücke auf Schießen und wähle einen Bereich auf dem rechten Spielfeld"; }));
                        tutorialTimer.Start();
                    }
                    gameTimer.Start();
                }
                else
                {
                    BubbleText.Dispatcher.BeginInvoke(new Action(() => { BubbleText.Text = "\r\nDu fängst an!!! ArrrRr"; }));
                    Thread.Sleep(1500);
                    RouletteToggleVisibility();
                    gameLogicState = GameLogicState.PlayerStart;
                    if (tutorial)
                    {
                        TutorialToggleVisibility();
                        tutorialState = TutorialState.FirstTurnPlayer;
                        tutorialTimer.Start();
                    }
                    gameTimer.Start();
                }
            }
            if (rouletteSpeed >= 20.0)
            {
                rouletteActive = true;
                rouletteStart = false;
            }
        }

        private void EnemyShot()
        {
            gameTimer.Stop();
            bool checkingShot = true;
            int x = rnd.Next(groundSize);
            int y = rnd.Next(groundSize);

            while (checkingShot)
            {
                x = rnd.Next(groundSize);
                y = rnd.Next(groundSize);

                foreach (Shot shot in shotsEnemy)
                {
                    if (shot.GetX() == x && shot.GetY() == y)
                    {
                        checkingShot = true;
                        break;
                    }
                    else
                    {
                        checkingShot = false;
                    }
                }

                if (shotsEnemy.Count == 0)
                {
                    checkingShot = false;
                }
            }
            shotsEnemy.Add(new Shot(x, y, groundSize, shipsPlayer, CanvasPlayer));
            if (shotsEnemy[shotsEnemy.Count - 1].IsHit())
            {
                HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Treffer!! Der Gegner darf nochmal schießen!!"; }));
                shootAgain = true;
                gameTimer.Start();
            }
            else
            {
                HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Leider daneben..."; }));
                gameLogicState = GameLogicState.PlayerShot;
                if (tutorialState == TutorialState.FirstTurnEnemy)
                {
                    tutorialState = TutorialState.FirstTurnPlayer;
                }
                if (tutorial)
                {
                    tutorialTimer.Start();
                }
                gameTimer.Start();
            }
        }

        private void PlayerShot()
        {
            if ((int)mouseOnEnemyField.X > 0 && (int)mouseOnEnemyField.Y > 0 && (int)mouseOnEnemyField.X < 700 && (int)mouseOnEnemyField.Y < 700)
            {
                int x = (int)mouseOnEnemyField.X / (700 / groundSize);
                int y = (int)mouseOnEnemyField.Y / (700 / groundSize);
                shooting = true;

                foreach (Shot shot in shotsPlayer)
                {
                    if (shot.GetX() == x && shot.GetY() == y)
                    {
                        shooting = false;
                        break;
                    }
                }
                if (shotsPlayer.Count == 0)
                {
                    shooting = true;
                }
                if (shooting)
                {
                    BtnShoot.Dispatcher.BeginInvoke(new Action(() => { BtnShoot.Visibility = Visibility.Hidden; }));
                    shotsPlayer.Add(new Shot(x, y, groundSize, shipsEnemy, CanvasEnemy));
                    if (shotsPlayer[shotsPlayer.Count - 1].IsHit())
                    {
                        HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Treffer!!! Du darfst nochmal schießen!!"; }));
                        gameLogicState = GameLogicState.PlayerShot;
                        shootAgain = true;
                    }
                    else
                    {
                        HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Leider daneben..."; }));
                        gameLogicState = GameLogicState.EnemyShot;
                    }

                    gameTimer.Start();
                    playerShoots = false;
                    Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["standard"]).Cursor;
                    TutorialEnd();
                }
            }
        }

        private void CheckWin(object sender, ElapsedEventArgs e)
        {
            bool deadP = false;
            bool deadE = false;
            foreach (Ship ship in shipsPlayer)
            {
                if (ship.IsDead)
                {
                    deadP = true;
                }
                else
                {
                    deadP = false;
                    break;
                }
            }
            foreach (Ship ship in shipsEnemy)
            {
                if (ship.IsDead)
                {
                    deadE = true;
                }
                else
                {
                    deadE = false;
                    break;
                }
            }
            if (deadP)
            {
                HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Der Gegner hat das Spiel gewonnen!!!!"; }));
                BtnShoot.Dispatcher.BeginInvoke(new Action(() => { BtnShoot.Visibility = Visibility.Hidden; }));
                Thread.Sleep(5000);
                Dispatcher.BeginInvoke(new Action(() => { Close(); }));
            }

            if (deadE)
            {
                HeadLine.Dispatcher.BeginInvoke(new Action(() => { HeadLine.Text = "Du hast das Spiel gewonnen!!!!!!"; }));
                BtnShoot.Dispatcher.BeginInvoke(new Action(() => { BtnShoot.Visibility = Visibility.Hidden; }));
                Thread.Sleep(5000);
                Dispatcher.BeginInvoke(new Action(() => { Close(); }));
            }
        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            rouletteActive = false;
            rouletteStart = false;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && BtnShoot.IsVisible)
            {
                Mouse.OverrideCursor = ((FrameworkElement)Application.Current.Resources["crosshair"]).Cursor;
                playerShoots = true;
            }
        }

        private void TutorialEnd()
        {
            SpeechBubble.Dispatcher.BeginInvoke(new Action(() => { SpeechBubble.Visibility = Visibility.Hidden; }));
            BubbleText.Dispatcher.BeginInvoke(new Action(() => { BubbleText.Visibility = Visibility.Hidden; }));
            Pirate.Dispatcher.BeginInvoke(new Action(() => { Pirate.Visibility = Visibility.Hidden; }));

            tutorialState = TutorialState.Stop;
            tutorialTimer.Stop();
        }

        private void TutorialToggleVisibility()
        {
            if (SpeechBubble.IsVisible && BubbleText.IsVisible && Pirate.IsVisible)
            {
                SpeechBubble.Dispatcher.BeginInvoke(new Action(() => { SpeechBubble.Visibility = Visibility.Hidden; }));
                BubbleText.Dispatcher.BeginInvoke(new Action(() => { BubbleText.Visibility = Visibility.Hidden; }));
                Pirate.Dispatcher.BeginInvoke(new Action(() => { Pirate.Visibility = Visibility.Hidden; }));
            }
            else
            {
                SpeechBubble.Dispatcher.BeginInvoke(new Action(() => { SpeechBubble.Visibility = Visibility.Visible; }));
                BubbleText.Dispatcher.BeginInvoke(new Action(() => { BubbleText.Visibility = Visibility.Visible; }));
                Pirate.Dispatcher.BeginInvoke(new Action(() => { Pirate.Visibility = Visibility.Visible; }));
            }
        }

        private void RouletteToggleVisibility()
        {
            if (compass.IsVisible && roulette.IsVisible & Dimmer.IsVisible)
            {
                compass.Dispatcher.BeginInvoke(new Action(() => { compass.Visibility = Visibility.Hidden; }));
                roulette.Dispatcher.BeginInvoke(new Action(() => { roulette.Visibility = Visibility.Hidden; }));
                Dimmer.Dispatcher.BeginInvoke(new Action(() => { Dimmer.Visibility = Visibility.Hidden; }));
            }
        }
    }
}